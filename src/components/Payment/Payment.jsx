import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export default function Payment({ payValue, setPayValue, onPayment }) {
    return (
        <div style={{marginTop:'20px'}}>
            <Typography variant="h5" gutterBottom>
                Payment
            </Typography>
            <FormControl fullWidth>
                <InputLabel htmlFor="adornment-amount">
                    Amount
                </InputLabel>
                <Input
                    id="adornment-amount"
                    value={payValue}
                    onChange={(e) => setPayValue(e.target.value)}
                    startAdornment={
                        <InputAdornment position="start">$</InputAdornment>
                    }
                />
            </FormControl>
            <Button
                style={{ marginTop: '10px' }}
                variant="contained"
                color="default"
                type="button"
                onClick={onPayment}
            >
                Pay
            </Button>
        </div>
    );
}
