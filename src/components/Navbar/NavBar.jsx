import React from 'react';
import {Link, useLocation} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {useStyles} from './styles';

export default function NavBar() {
    const classes = useStyles();
    const location = useLocation();

    return (
        <div>
            <AppBar position="fixed" color="primary">
                <Toolbar>
                    <Typography variant="h5" className={classes.title}>
                        Loan App                       
                    </Typography>
                    {location.pathname === '/home' && (
                        <Link to="/sign" >
                            <div className={classes.cartIcon}>Out</div>
                        </Link>
                    )}
                </Toolbar>
            </AppBar>
        </div>
    );
}
