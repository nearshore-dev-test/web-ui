import { makeStyles } from '@material-ui/core/styles';


export const useStyles = makeStyles((theme) => ({
    cartIcon:{
        color: 'white',
        fontSize: '130%',
        textDecoration: 'none',
    },
    appBar: {
        // marginBottom: '30px'
        // boxShadow: 'none',
        // borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
        // [theme.breakpoints.up('sm')]: {
        //     width: `calc(100% - ${drawerWidth}px)`,
        //     marginLeft: drawerWidth,
        // },
    },
    title: {
        flexGrow: 1,
        alignItems: 'center',
        display: 'flex',
        textDecoration: 'none',
    }
}));