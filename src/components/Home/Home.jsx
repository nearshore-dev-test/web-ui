import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ApplyForLoan from '../ApplyForLoan/AppyForLoan';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import * as customerService from '../../services/customerService';
import Payment from '../Payment/Payment';

export default function Home({ user }) {
    const [currentAmount, setCurrentAmount] = useState(0);
    const [loanValue, setLoanValue] = useState(0);
    const [payValue, setPayValue] = useState(0);
    const [openSnack, setOpenSnack] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');
    
    useEffect(() => {
        setCurrentAmount(user.amount);
    }, []);

    const handleApplyForLoan = async () => {
        try {
            const {data} = await customerService.aplyForLoan(user.email, loanValue);
            if (data.email && data.amount) return setCurrentAmount(data.amount);
        } catch (error) {
            const { response } = error;
            setErrorMsg(response.data.message);
            setOpenSnack(true);
        }
    };

    const handlePayment = async () => {
        try {
            const {data} = await customerService.payment(user.email, payValue);
            if (data.email) return setCurrentAmount(data.amount);
        } catch (error) {            
            const { response } = error;
            setErrorMsg(response.data.message);
            setOpenSnack(true);
        }
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') return;

        setOpenSnack(false);
    };

    return (
        <div>
            <Grid container justify="center" spacing={4}>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                    <Typography variant="h5" gutterBottom>Current State</Typography>
                    <Typography variant="subtitle1" color="initial">User: {user && <strong>{user.email}</strong>}</Typography>                
                    <Typography variant="subtitle1" color="initial">Current Amount: {user && <strong>{currentAmount}</strong> }</Typography>
                </Grid>
                <br/>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                    <ApplyForLoan onAppyForLoan={handleApplyForLoan} value={loanValue} setLoanValue={setLoanValue}/>
                    <Payment payValue={payValue} setPayValue={setPayValue} onPayment={handlePayment}/>
                </Grid>
            </Grid>
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'left', }}
                open={openSnack}
                autoHideDuration={3000}
                onClose={handleClose}                
            >
                <MuiAlert onClose={handleClose} elevation={6} variant="filled" severity="error">
                    {errorMsg}
                </MuiAlert>
            </Snackbar>
        </div>
    );
}
