import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export default function AppyForLoan({onAppyForLoan, value, setLoanValue}) {
    return (
        <div>
            <Typography variant="h5" gutterBottom>Apply for a Loan</Typography>
            <FormControl fullWidth >
                <InputLabel htmlFor="standard-adornment-amount">Amount</InputLabel>
                <Input
                    id="standard-adornment-amount"
                    value={value}
                    onChange={(e)=>setLoanValue(e.target.value)}
                    startAdornment={<InputAdornment position="start">$</InputAdornment>}
                />
            </FormControl>
            <Button style={{marginTop: '10px'}} variant="contained" color="default" type="button" onClick={onAppyForLoan}>
                Apply
            </Button>
        </div>
    );
}
