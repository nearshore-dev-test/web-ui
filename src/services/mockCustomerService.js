const db = [];
const maxLoan = 1000;

const createCustomer = (email, amount=0) => {
    const customer = {email, amount};
    db.push(customer);
    
    return customer;
};

export const aplyForLoan = (email, amount=0) => {
    const customerInDb =  db.find(c => c.email === email);
    if(!customerInDb) return createCustomer(email);
    
    const newLoanQuantity = +amount + +customerInDb.amount;
    if(newLoanQuantity>maxLoan) return {customCode: 100, message:'total greater than 1000$'};

    customerInDb.amount = newLoanQuantity;
    return customerInDb;
};

export const payment = (email, payment) => {
    const customerInDb =  db.find(c => c.email === email);
    if(!customerInDb) return {message: 'user not found'};

    const debt = customerInDb.amount;
    
    if (debt === 0) return {message: 'Cannot accept, debt is zero'};
    if ( payment > debt) return {message: 'Cannot accept, payment is greater than debt'};

    const newAmount = debt - payment;
    customerInDb.amount = newAmount;
    return customerInDb;

};