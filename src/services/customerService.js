import axios from 'axios';

const apiUrl = process.env.REACT_APP_API_URL;

export const aplyForLoan = (email, amount = 0) => {
    const customer = {email, amount};
    return axios.post(`${apiUrl}/loan`,customer);
};

export const payment = (email, payment) => {
    const customer = {email, amount:payment};
    return axios.post(`${apiUrl}/payment`, customer);
};
