import React, {useState} from 'react';
import NavBar from './components/Navbar/NavBar';
import { Switch, Route, Redirect} from 'react-router';
import Signin from './components/SignIn/Signin';
import Home from './components/Home/Home';
import * as customerService from './services/customerService';
import './App.css';


export default function App() {   

    const [user, setUser] = useState({}) ;

    const handleLogin = async (email) => {
        try {
            const {data} = await customerService.aplyForLoan(email);
            setUser(data);
        } catch (error) {
            const { response } = error;
            console.log(response);
        }
    };

    return (
        <>
            <NavBar/>
            <div className="app-body">
                <Switch>
                    <Route path="/sign" render={props => <Signin {...props} handleLogin={handleLogin}/>}/>
                    <Route path="/home" render={props => <Home {...props} user={user}/>}/>
                    <Redirect from="/" exact to="/sign" />
                    <Redirect to="/" />
                </Switch>
            </div>
        </>
    );
}
