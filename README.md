**Instructions**

- Clone the repository :
    `git clone https://gitlab.com/nearshore-dev-test/web-ui.git`

- Install the dependencies: 
    `cd web-ui && yarn install` or `cd web-ui && npm install`

- Make sure the Backend is running, see
    [https://gitlab.com/nearshore-dev-test/backend](https://gitlab.com/nearshore-dev-test/backend) .

- Create a .env file and define an env var for the API URL, the Backend PORT by default is 4000):

    `eg: REACT_APP_API_URL=http://localhost:{PORT}`

- Start the project:

    ` yarn start` or `npm start`

- Open your browser on port 3000 (http://localhost:3000)
