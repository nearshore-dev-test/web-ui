import {shallow } from 'enzyme';
import React from 'react';
import Signin from '../../src/components/SignIn/Signin';


it('should render Signin Component', ()=>{
    const app = shallow(<Signin/>);
    
    expect(app).toMatchSnapshot();
});